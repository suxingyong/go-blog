package common

import (
	"encoding/json"
	"go-blog/models"
	"go-blog/my-frame/config"
	"io/ioutil"
	"net/http"
	"sync"
)

var Template models.HtmlTemplate

func LoadTemplate() {
	wg := sync.WaitGroup{}
	var err error
	wg.Add(1)
	go func() {
		Template, err = models.InitTemplate(config.Cfg.System.CurrentDir + "/template")
		if err != nil {
			panic(err)
		}
		wg.Done()
	}()
	wg.Wait()
}

func Success(w http.ResponseWriter, data interface{}) {
	var result models.Result
	result.Code = 200
	result.Data = data
	resultBytes, _ := json.Marshal(result)
	w.Header().Set("Content-Type", "application/json")
	w.Write(resultBytes)
}

func Error(w http.ResponseWriter, err error) {
	var result models.Result
	result.Code = 500
	result.Error = err.Error()
	resultBytes, _ := json.Marshal(result)
	w.Header().Set("Content-Type", "application/json")
	w.Write(resultBytes)
}

func GetRequestJsonParam(r *http.Request) map[string]interface{} {
	var params map[string]interface{}
	body, _ := ioutil.ReadAll(r.Body)
	_ = json.Unmarshal(body, &params)
	return params
}
