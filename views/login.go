package views

import (
	"go-blog/common"
	"go-blog/my-frame/config"
	"net/http"
)

func (h *HtmlApi) Login(w http.ResponseWriter, r *http.Request) {
	login := common.Template.Login

	login.WriteData(w, config.Cfg.Viewer)
}
