package views

import (
	"errors"
	"go-blog/common"
	"go-blog/service"
	"log"
	"net/http"
	"strconv"
	"strings"
)

func (h *HtmlApi) Category(w http.ResponseWriter, r *http.Request) {
	categoryTemplate := common.Template.Category
	//http://localhost:8080/c/1 1为参数，是分类的id
	path := r.URL.Path
	cIdStr := strings.TrimPrefix(path, "/c/")
	cId, err := strconv.Atoi(cIdStr)
	if err != nil {
		categoryTemplate.WriteError(w, errors.New("不识别此请求路径"))
		return
	}
	//表单解析
	if err = r.ParseForm(); err != nil {
		log.Printf("表单获取失败：%s", err.Error())
		categoryTemplate.WriteError(w, errors.New("系统错误，请联系管理员"))
		return
	}
	pageStr := r.Form.Get("page")
	//页数
	page := 1
	if pageStr != "" {
		page, err = strconv.Atoi(pageStr)
		//后期可忽略
		if err != nil {
			log.Printf("数据转换错误：%s", err.Error())
			categoryTemplate.WriteError(w, errors.New("系统错误，请联系管理员"))
			return
		}
	}
	pageSizeStr := r.Form.Get("pageSize")
	//每页数据
	pageSize := 10
	if pageSizeStr != "" {
		pageSize, err = strconv.Atoi(pageSizeStr)
		//后期可忽略
		if err != nil {
			log.Printf("数据转换错误：%s", err.Error())
			categoryTemplate.WriteError(w, errors.New("系统错误，请联系管理员"))
			return
		}
	}
	categoryResponse, err := (&service.CategoryService{}).GetPostsByCategoryId(cId, page, pageSize)
	if err != nil {
		categoryTemplate.WriteError(w, err)
		return
	}
	categoryTemplate.WriteData(w, categoryResponse)
}
