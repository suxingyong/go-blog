package views

import (
	"go-blog/common"
	"go-blog/service"
	"net/http"
)

func (h *HtmlApi) Pigeonhole(w http.ResponseWriter, r *http.Request) {
	pigeonhole := common.Template.Pigeonhole

	pigeonholeRes := (&service.PostService{}).FindPostPigeonhole()
	pigeonhole.WriteData(w, pigeonholeRes)
}
