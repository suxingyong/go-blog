package views

import (
	"errors"
	"go-blog/common"
	"go-blog/service"
	"log"
	"net/http"
	"strconv"
	"strings"
)

//func (h *HtmlApi) Index(w http.ResponseWriter, r *http.Request) {
//	index := common.Template.Index
//
//	//页面上涉及到的所有的数据，必须有定义
//	//数据库查询
//	//表单解析
//	if err := r.ParseForm(); err != nil {
//		log.Printf("表单获取失败：%s", err.Error())
//		index.WriteError(w, errors.New("系统错误，请联系管理员"))
//		return
//	}
//	var err error
//	pageStr := r.Form.Get("page")
//	//页数
//	page := 1
//	if pageStr != "" {
//		page, err = strconv.Atoi(pageStr)
//		//后期可忽略
//		if err != nil {
//			log.Printf("数据转换错误：%s", err.Error())
//			index.WriteError(w, errors.New("系统错误，请联系管理员"))
//			return
//		}
//	}
//	pageSizeStr := r.Form.Get("pageSize")
//	//每页数据
//	pageSize := 10
//	if pageSizeStr != "" {
//		pageSize, err = strconv.Atoi(pageSizeStr)
//		//后期可忽略
//		if err != nil {
//			log.Printf("数据转换错误：%s", err.Error())
//			index.WriteError(w, errors.New("系统错误，请联系管理员"))
//			return
//		}
//	}
//
//	hr, err := service.GetAllIndexInfo(page, pageSize)
//	if err != nil {
//		log.Println("Index获取数据出错：", err)
//		index.WriteError(w, errors.New("系统错误，请联系管理员"))
//	}
//	err = index.WriteData(w, hr)
//	if err != nil {
//		log.Println(err)
//	}
//}

func (h *HtmlApi) Index(w http.ResponseWriter, r *http.Request) {
	index := common.Template.Index

	//页面上涉及到的所有的数据，必须有定义
	//数据库查询
	//表单解析
	if err := r.ParseForm(); err != nil {
		log.Printf("表单获取失败：%s", err.Error())
		index.WriteError(w, errors.New("系统错误，请联系管理员"))
		return
	}
	var err error
	pageStr := r.Form.Get("page")
	//页数
	page := 1
	if pageStr != "" {
		page, err = strconv.Atoi(pageStr)
		//后期可忽略
		if err != nil {
			log.Printf("数据转换错误：%s", err.Error())
			index.WriteError(w, errors.New("系统错误，请联系管理员"))
			return
		}
	}
	pageSizeStr := r.Form.Get("pageSize")
	//每页数据
	pageSize := 10
	if pageSizeStr != "" {
		pageSize, err = strconv.Atoi(pageSizeStr)
		//后期可忽略
		if err != nil {
			log.Printf("数据转换错误：%s", err.Error())
			index.WriteError(w, errors.New("系统错误，请联系管理员"))
			return
		}
	}

	path := r.URL.Path
	slug := strings.TrimPrefix(path, "/")
	hr, err := (&service.PostService{}).GetAllIndexInfoBySlug(slug, page, pageSize)
	if err != nil {
		log.Println("Index获取数据出错：", err)
		index.WriteError(w, errors.New("系统错误，请联系管理员"))
	}
	err = index.WriteData(w, hr)
	if err != nil {
		log.Println(err)
	}
}
