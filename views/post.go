package views

import (
	"errors"
	"go-blog/common"
	"go-blog/service"
	"log"
	"net/http"
	"strconv"
	"strings"
)

func (h *HtmlApi) Detail(w http.ResponseWriter, r *http.Request) {
	detail := common.Template.Detail

	//获取路径参数
	path := r.URL.Path
	pIdStr := strings.TrimPrefix(path, "/p/")
	//7.html
	pIdStr = strings.TrimSuffix(pIdStr, ".html")
	pid, err := strconv.Atoi(pIdStr)
	if err != nil {
		msg := "不识别此请求路径"
		log.Printf("%s,err:%s", msg, err.Error())
		detail.WriteError(w, errors.New(msg))
		return
	}
	postRes, err := (&service.PostService{}).GetPostDetail(pid)
	if err != nil {
		msg := "查询出错"
		log.Printf("%s,err:%s", msg, err.Error())
		detail.WriteError(w, errors.New(msg))
		return
	}
	detail.WriteData(w, postRes)
}

func (h *HtmlApi) Writing(w http.ResponseWriter, r *http.Request) {
	writing := common.Template.Writing
	wr := (&service.PostService{}).Writing()
	writing.WriteData(w, wr)

}
