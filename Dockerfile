FROM alpine
WORKDIR /Initial
COPY ./target/go-blog .
COPY ./config/config.toml .
RUN  mkdir config && mv config.toml config/config.toml
EXPOSE 8080 8888
ENTRYPOINT ["./go-blog"]