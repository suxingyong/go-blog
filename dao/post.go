package dao

import (
	"fmt"
	"go-blog/models"
	"go-blog/my-frame/dao/mysql"
	"log"
)

type PostDao struct {
}

func (p *PostDao) CountGetAllPost() int {
	row := mysql.DB.QueryRow("select count(1) from blog_post")
	if row.Err() != nil {
		log.Printf("CountGetAllPost 查询出错：%s", row.Err().Error())
		return 0
	}
	count := 0
	err := row.Scan(&count)
	if err != nil {
		log.Printf("CountGetAllPost 取值出错：%s", err.Error())
		return 0
	}
	return count
}

func (p *PostDao) CountGetAllPostBySlug(slug string) int {
	row := mysql.DB.QueryRow("select count(1) from blog_post where slug=?", slug)
	if row.Err() != nil {
		log.Printf("CountGetAllPost 查询出错：%s", row.Err().Error())
		return 0
	}
	count := 0
	err := row.Scan(&count)
	if err != nil {
		log.Printf("CountGetAllPost 取值出错：%s", err.Error())
		return 0
	}
	return count
}

func (p *PostDao) CountGetAllPostByCategoryId(categoryId int) int {
	row := mysql.DB.QueryRow("select count(1) from blog_post where category_id=?", categoryId)
	if row.Err() != nil {
		log.Printf("CountGetAllPostByCategoryId 查询出错：%s", row.Err().Error())
		return 0
	}
	count := 0
	err := row.Scan(&count)
	if err != nil {
		log.Printf("CountGetAllPostByCategoryId 取值出错：%s", err.Error())
		return 0
	}
	return count
}

func (p *PostDao) GetPostAll() ([]models.Post, error) {
	rows, err := mysql.DB.Query("select * from blog_post")
	if err != nil {
		log.Printf("查询文章信息出错：%s", err.Error())
		return nil, err
	}
	var posts []models.Post
	for rows.Next() {
		var post models.Post
		err = rows.Scan(
			&post.Pid,
			&post.Title,
			&post.Content,
			&post.Markdown,
			&post.CategoryId,
			&post.UserId,
			&post.ViewCount,
			&post.Type,
			&post.Slug,
			&post.CreateAt,
			&post.UpdateAt,
		)
		if err != nil {
			log.Printf("post 取值出错：%s", err.Error())
			return nil, err
		}
		posts = append(posts, post)
	}
	return posts, nil
}

func (p *PostDao) GetPostPage(page, pageSize int) ([]models.Post, error) {
	page = (page - 1) * pageSize
	rows, err := mysql.DB.Query("select * from blog_post limit ?,?", page, pageSize)
	if err != nil {
		log.Printf("查询文章分页信息出错：%s", err.Error())
		return nil, err
	}
	var posts []models.Post
	for rows.Next() {
		var post models.Post
		err = rows.Scan(
			&post.Pid,
			&post.Title,
			&post.Content,
			&post.Markdown,
			&post.CategoryId,
			&post.UserId,
			&post.ViewCount,
			&post.Type,
			&post.Slug,
			&post.CreateAt,
			&post.UpdateAt,
		)
		if err != nil {
			log.Printf("post 取值出错：%s", err.Error())
			return nil, err
		}
		posts = append(posts, post)
	}
	return posts, nil
}

func (p *PostDao) GetPostPageBySlug(slug string, page, pageSize int) ([]models.Post, error) {
	page = (page - 1) * pageSize
	rows, err := mysql.DB.Query("select * from blog_post where slug=? limit ?,?", slug, page, pageSize)
	if err != nil {
		log.Printf("查询文章分页信息出错：%s", err.Error())
		return nil, err
	}
	var posts []models.Post
	for rows.Next() {
		var post models.Post
		err = rows.Scan(
			&post.Pid,
			&post.Title,
			&post.Content,
			&post.Markdown,
			&post.CategoryId,
			&post.UserId,
			&post.ViewCount,
			&post.Type,
			&post.Slug,
			&post.CreateAt,
			&post.UpdateAt,
		)
		if err != nil {
			log.Printf("post 取值出错：%s", err.Error())
			return nil, err
		}
		posts = append(posts, post)
	}
	return posts, nil
}

func (p *PostDao) GetPostPageByCategoryId(categoryId, page, pageSize int) ([]models.Post, error) {
	page = (page - 1) * pageSize
	rows, err := mysql.DB.Query("select * from blog_post where category_id=? limit ?,?", categoryId, page, pageSize)
	if err != nil {
		log.Printf("查询文章分页信息出错：%s", err.Error())
		return nil, err
	}
	var posts []models.Post
	for rows.Next() {
		var post models.Post
		err = rows.Scan(
			&post.Pid,
			&post.Title,
			&post.Content,
			&post.Markdown,
			&post.CategoryId,
			&post.UserId,
			&post.ViewCount,
			&post.Type,
			&post.Slug,
			&post.CreateAt,
			&post.UpdateAt,
		)
		if err != nil {
			log.Printf("post 取值出错：%s", err.Error())
			return nil, err
		}
		posts = append(posts, post)
	}
	return posts, nil
}

// GetPostById 根据id获取文章详情
func (p *PostDao) GetPostById(pid int) (*models.Post, error) {
	//row := mysql.DB.QueryRow("select * from blog_post where pid=?", pid)
	//if row.Err() != nil {
	//	log.Printf("GetPostById 查询出错：%s", row.Err().Error())
	//	return nil, row.Err()
	//}
	//
	//var post models.Post
	//err := row.Scan(
	//	&post.Pid,
	//	&post.Title,
	//	&post.Content,
	//	&post.Markdown,
	//	&post.CategoryId,
	//	&post.UserId,
	//	&post.ViewCount,
	//	&post.Type,
	//	&post.Slug,
	//	&post.CreateAt,
	//	&post.UpdateAt,
	//)
	//if err != nil {
	//	log.Printf("post 取值出错：%s", err.Error())
	//	return nil, err
	//}
	//return &post, nil
	var post models.Post
	err := mysql.DB.GetOne(&post, "select * from blog_post where pid=?", pid)
	if err != nil {
		log.Printf("post 查询出错：%s", err.Error())
		return nil, err
	}
	return &post, nil
}

// GetPostSearch 根据条件查询
func (p *PostDao) GetPostSearch(condition string) ([]models.Post, error) {
	rows, err := mysql.DB.Query("select * from blog_post where title like ?", fmt.Sprintf("%%%s%%", condition))
	if err != nil {
		log.Printf("查询文章信息出错：%s", err.Error())
		return nil, err
	}
	var posts []models.Post
	for rows.Next() {
		var post models.Post
		err = rows.Scan(
			&post.Pid,
			&post.Title,
			&post.Content,
			&post.Markdown,
			&post.CategoryId,
			&post.UserId,
			&post.ViewCount,
			&post.Type,
			&post.Slug,
			&post.CreateAt,
			&post.UpdateAt,
		)
		if err != nil {
			log.Printf("post 取值出错：%s", err.Error())
			return nil, err
		}
		posts = append(posts, post)
	}
	return posts, nil
}

// SavePost 保存文章数据(新增)
func (p *PostDao) SavePost(post *models.Post) {
	ret, err := mysql.DB.Exec("insert into blog_post "+
		"(title,content,markdown,category_id,user_id,view_count,type,slug,create_at,update_at) "+
		"values (?,?,?,?,?,?,?,?,?,?)",
		post.Title,
		post.Content,
		post.Markdown,
		post.CategoryId,
		post.UserId,
		post.ViewCount,
		post.Type,
		post.Slug,
		post.CreateAt,
		post.UpdateAt)
	if err != nil {
		log.Printf("insert into blog_post err:%s", err.Error())
		return
	}
	pid, err := ret.LastInsertId()
	if err != nil {
		log.Printf("ret.LastInsertId().err:%s", err.Error())
		return
	}
	post.Pid = int(pid)
}

// UpdatePost 更新文章数据
func (p *PostDao) UpdatePost(post *models.Post) {
	ret, err := mysql.DB.Exec("update blog_post set "+
		"title=?,content=?,markdown=?,category_id=?,user_id=?,view_count=?,type=?,slug=?,update_at=? "+
		"where pid=?",
		post.Title,
		post.Content,
		post.Markdown,
		post.CategoryId,
		post.UserId,
		post.ViewCount,
		post.Type,
		post.Slug,
		post.UpdateAt,
		post.Pid)
	if err != nil {
		log.Printf("update blog_post err:%s", err.Error())
		return
	}
	pid, err := ret.RowsAffected()
	if err != nil {
		log.Printf("ret.RowsAffected().err:%s", err.Error())
		return
	}
	post.Pid = int(pid)
}
