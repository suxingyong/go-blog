package dao

import (
	"go-blog/models"
	"go-blog/my-frame/dao/mysql"
	"log"
)

type UserDao struct {
}

func (u *UserDao) GetUserNameById(userId int) string {
	row := mysql.DB.QueryRow("select user_name from blog_user where uid=?", userId)
	if row.Err() != nil {
		log.Printf("GetUserNameById 查询出错：%s", row.Err().Error())
		return ""
	}
	userName := ""
	err := row.Scan(&userName)
	if err != nil {
		log.Printf("GetUserNameById 取值出错：%s", err.Error())
		return ""
	}
	return userName
}

func (u *UserDao) GetUser(userName, passwd string) *models.User {
	row := mysql.DB.QueryRow("select * from blog_user where user_name=? and passwd=?", userName, passwd)
	if row.Err() != nil {
		log.Printf("GetUser 查询出错：%s", row.Err().Error())
		return nil
	}
	var user models.User
	err := row.Scan(&user.Uid, &user.UserName, &user.Passwd, &user.Avatar, &user.CreateAt, &user.UpdateAt)
	if err != nil {
		log.Printf("GetUser 取值出错：%s", err.Error())
		return nil
	}
	return &user
}
