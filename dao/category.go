package dao

import (
	"go-blog/models"
	"go-blog/my-frame/dao/mysql"
	"log"
)

type CategoryDao struct {
}

func (c *CategoryDao) GetCategoryNameById(categoryId int) string {
	row := mysql.DB.QueryRow("select name from blog_category where cid=?", categoryId)
	if row.Err() != nil {
		log.Printf("GetCategoryNameById 查询出错：%s", row.Err().Error())
		return ""
	}
	categoryName := ""
	err := row.Scan(&categoryName)
	if err != nil {
		log.Printf("GetCategoryNameById 取值出错：%s", err.Error())
		return ""
	}
	return categoryName
}

func (c *CategoryDao) GetAllCategory() ([]models.Category, error) {
	rows, err := mysql.DB.Query("select * from blog_category")
	if err != nil {
		log.Printf("GetAllCategory 查询出错：%s", err.Error())
		return nil, err
	}
	var categorys []models.Category
	for rows.Next() {
		var category models.Category
		err = rows.Scan(
			&category.Cid,
			&category.Name,
			&category.CreateAt,
			&category.UpdateAt,
		)
		if err != nil {
			log.Printf("GetAllCategory 取值出错：%s", err.Error())
			return nil, err
		}
		categorys = append(categorys, category)
	}
	return categorys, nil
}
