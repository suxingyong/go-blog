package router

import (
	"go-blog/api"
	"go-blog/views"
	"net/http"
)

func Router() {
	//1. 页面 views
	//2. 数据(json) api
	//3. 静态资源

	//首页
	http.HandleFunc("/", views.HTML.Index)
	//http://localhost:8080/c/1 1为参数，是分类的id
	http.HandleFunc("/c/", views.HTML.Category)

	//登录页面
	http.HandleFunc("/login", views.HTML.Login)

	//文章详情
	http.HandleFunc("/p/", views.HTML.Detail)

	//文章写作
	http.HandleFunc("/writing/", views.HTML.Writing)

	//归档
	http.HandleFunc("/pigeonhole", views.HTML.Pigeonhole)

	//登录交互
	http.HandleFunc("/api/v1/login", api.API.Login)

	//保存或者更新文章
	http.HandleFunc("/api/v1/post", api.API.SaveAndUpdatePost)

	//获取文章详情数据(更新时)[GET]
	http.HandleFunc("/api/v1/post/", api.API.GetPost)

	//搜索
	http.HandleFunc("/api/v1/post/search", api.API.SearchPost)

	http.Handle("/resource/", http.StripPrefix("/resource/", http.FileServer(http.Dir("public/resource/"))))
}
