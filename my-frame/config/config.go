package config

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"os"
	"strings"
)

// MyConfig 我的自定义配置
type MyConfig struct {
	Viewer Viewer       //页面配置
	System SystemConfig //系统配置
	Mysql  MysqlConfig  //mysql配置
}

// Viewer 页面配置
type Viewer struct {
	Title       string   //标题
	Description string   //描述
	Logo        string   //logo
	Navigation  []string //导航栏
	Bilibili    string   //B站访问链接
	Avatar      string   //
	UserName    string   //用户名
	UserDesc    string   //用户描述|用户个性签名
}

// SystemConfig 系统配置
type SystemConfig struct {
	AppName         string  //app名称
	Version         float32 //版本号
	CurrentDir      string  //工作根目录
	CdnURL          string  //cdn的访问url
	QiniuAccessKey  string  //七牛云的accessKey
	QiniuSecretKey  string  //七牛云的ecretKey
	Valine          bool    //Valine
	ValineAppid     string  //Valine的appid
	ValineAppkey    string  //Valine的appkey
	ValineServerURL string  //Valine的服务地址

	WebIp   string //web服务ip
	WebPort string //web端口号
}

// MysqlConfig mysql配置
type MysqlConfig struct {
	UserName        string //用户名
	UserPasswd      string //用户密码
	Ip              string //服务ip
	Port            string //服务端口号
	MaxIdleConns    int    //最大空闲连接数
	MaxOpenConns    int    //最大连接数
	ConnMaxLifetime int    //连接最大存活时间(秒)
	ConnMaxIdleTime int    //空闲连接最大存活时间(秒)
}

var Cfg *MyConfig

func init() {
	Cfg = new(MyConfig)

	//获取当前工作目录
	currentDir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	//TODO 如何取到根目录的地址?? 还有没有更简洁的办法
	Cfg.System.CurrentDir = strings.TrimPrefix(currentDir, "/my-frame/config/")
	Cfg.System.AppName = "go-blog"
	Cfg.System.Version = 1.0

	//从根目录下读取
	_, err = toml.DecodeFile(fmt.Sprintf("%s/config/config.toml", Cfg.System.CurrentDir), &Cfg)
	if err != nil {
		panic(err)
	}
	//log.Printf("metaData: %#v", metaData)
}
