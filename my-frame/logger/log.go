package logger

import (
	"log"
)

type MyLogger struct {
	*log.Logger
}

var Log MyLogger

//初始化日志包
func init() {
	//Log = MyLogger{
	//	Logger: log.New(os.Stdout, "go-blog", log.Ldate|log.Ltime|log.Lmicroseconds|log.Llongfile),
	//}
	Log = MyLogger{
		Logger: log.Default(),
	}
}
