package dao

// Dao 数据库交互层
type Dao interface {
	// GetOne 查询一条数据
	GetOne(model interface{}, sql string, args ...interface{}) error
	// Find 查询多条
	Find(model interface{}, sql string, args ...interface{}) error
	// Count 统计
	Count(count *int, sql string, args ...interface{}) error
	// Save 新增
	Save(sql string, args ...interface{}) (int64, error)
	// Update 修改
	Update(sql string, args ...interface{}) (int64, error)
	// Delete 删除
	Delete(sql string, args ...interface{}) (int64, error)
}
