package mysql

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"go-blog/my-frame/config"
	"go-blog/my-frame/logger"
	"log"
	"net/url"
	"reflect"
	"strconv"
	"time"
)

// MySqlDB 自定义的关系型数据交互
type MySqlDB struct {
	*sql.DB
}

// DB 全局通用DB
var DB MySqlDB

func init() {
	//执行main之前 先执行init方法
	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s:%s)/goblog?charset=utf8&loc=%s&parseTime=true",
		config.Cfg.Mysql.UserName,
		config.Cfg.Mysql.UserPasswd,
		config.Cfg.Mysql.Ip,
		config.Cfg.Mysql.Port,
		url.QueryEscape("Asia/Shanghai"))
	db, err := sql.Open("mysql", dataSourceName)
	if err != nil {
		log.Println("连接数据库异常")
		panic(err)
	}
	//最大空闲连接数，默认不设置，是2个最大空闲连接
	db.SetMaxIdleConns(config.Cfg.Mysql.MaxIdleConns)
	//最大连接数，默认不设置，是不限制最大连接数
	db.SetMaxOpenConns(config.Cfg.Mysql.MaxOpenConns)
	//连接最大存活时间
	db.SetConnMaxLifetime(time.Second * time.Duration(config.Cfg.Mysql.ConnMaxLifetime))
	//空闲连接最大存活时间
	db.SetConnMaxIdleTime(time.Minute * time.Duration(config.Cfg.Mysql.ConnMaxIdleTime))
	err = db.Ping()
	if err != nil {
		log.Println("数据库无法连接")
		_ = db.Close()
		panic(err)
	}
	log.Println("mysql数据库连接成功！")
	DB = MySqlDB{
		DB: db,
	}
}

// GetOne 查询一个
func (d *MySqlDB) GetOne(model interface{}, sql string, args ...interface{}) error {
	//执行查询
	rows, err := d.Query(sql, args...)
	if err != nil {
		logger.Log.Printf("d.Query.err:%s", err.Error())
		return err
	}
	//TODO 获取 列名??
	columnNames, err := rows.Columns()
	if err != nil {
		logger.Log.Printf("rows.Columns.err:%s", err.Error())
		return err
	}
	//将列名一一对应
	values := make([][]byte, len(columnNames))
	scans := make([]interface{}, len(columnNames))
	for i := range values {
		//
		scans[i] = &values[i]
	}
	if rows.Next() {
		//将查询到的值扫描到 scans 中
		err := rows.Scan(scans...)
		if err != nil {
			logger.Log.Printf("rows.Scan.err:%s", err.Error())
			return err
		}
	}

	//结果集 key为列名, value为列具体的值
	var resultMap = make(map[string]interface{})
	for i, column := range columnNames {
		resultMap[column] = string(values[i])
	}

	//利用反射将数据库读取出来的字段映射到结构体中
	elem := reflect.ValueOf(model).Elem()
	for i := 0; i < elem.NumField(); i++ {
		//结构体字段
		structField := elem.Type().Field(i)
		//映射到数据库的字段名称
		fieldName := structField.Tag.Get("json")
		//根据字段名称 取值
		value := resultMap[fieldName]
		//字段类型
		fieldType := structField.Type
		switch fieldType.String() {
		case "int":
			//TODO 这样转换是否属于多余???
			s := value.(string)
			vInt, _ := strconv.Atoi(s)
			elem.Field(i).Set(reflect.ValueOf(vInt))
		case "int8":
			s := value.(string)
			vInt8, _ := strconv.ParseInt(s, 10, 8)
			elem.Field(i).Set(reflect.ValueOf(vInt8))
		case "int16":
			s := value.(string)
			vInt16, _ := strconv.ParseInt(s, 10, 16)
			elem.Field(i).Set(reflect.ValueOf(vInt16))
		case "int32":
			s := value.(string)
			vInt32, _ := strconv.ParseInt(s, 10, 32)
			elem.Field(i).Set(reflect.ValueOf(vInt32))
		case "int64":
			s := value.(string)
			vInt64, _ := strconv.ParseInt(s, 10, 64)
			elem.Field(i).Set(reflect.ValueOf(vInt64))
		case "float32":
			s := value.(string)
			vFloat32, _ := strconv.ParseFloat(s, 32)
			elem.Field(i).Set(reflect.ValueOf(vFloat32))
		case "float64":
			s := value.(string)
			vFloat64, _ := strconv.ParseFloat(s, 64)
			elem.Field(i).Set(reflect.ValueOf(vFloat64))
		case "string":
			elem.Field(i).Set(reflect.ValueOf(value.(string)))
		case "time.Time":
			s := value.(string)
			vTime, _ := time.Parse(time.RFC3339, s)
			elem.Field(i).Set(reflect.ValueOf(vTime))
		}
	}

	return nil
}

// Find 查询多条 TODO 如何实现??
func (d *MySqlDB) Find(model interface{}, sql string, args ...interface{}) error {
	//执行查询
	rows, err := d.Query(sql, args...)
	if err != nil {
		logger.Log.Printf("d.Query.err:%s", err.Error())
		return err
	}
	//TODO 获取 列名??
	columnNames, err := rows.Columns()
	if err != nil {
		logger.Log.Printf("rows.Columns.err:%s", err.Error())
		return err
	}

	list := make([][]interface{}, len(columnNames))
	for rows.Next() {
		//将列名一一对应
		values := make([][]byte, len(columnNames))
		scans := make([]interface{}, len(columnNames))
		for i := range values {
			//
			scans[i] = &values[i]
		}
		//将查询到的值扫描到 scans 中
		err := rows.Scan(scans...)
		if err != nil {
			logger.Log.Printf("rows.Scan.err:%s", err.Error())
			return err
		}
		list = append(list, scans)
	}

	//结果集 key为列名, value为列具体的值
	var resultMap = make(map[string][]interface{})
	for i, column := range columnNames {
		resultMap[column] = append(resultMap[column], list[i]...)
	}

	//利用反射将数据库读取出来的字段映射到结构体中
	elem := reflect.ValueOf(model).Elem()
	for i := 0; i < elem.NumField(); i++ {
		//结构体字段
		structField := elem.Type().Field(i)
		//映射到数据库的字段名称
		fieldName := structField.Tag.Get("json")
		//根据字段名称 取值
		values := resultMap[fieldName]
		for j := range values {
			//字段类型
			fieldType := structField.Type
			switch fieldType.String() {
			case "int":
				//TODO 这样转换是否属于多余???
				s := values[j].(string)
				vInt, _ := strconv.Atoi(s)
				elem.Field(i).Set(reflect.ValueOf(vInt))
			case "int8":
				s := values[j].(string)
				vInt8, _ := strconv.ParseInt(s, 10, 8)
				elem.Field(i).Set(reflect.ValueOf(vInt8))
			case "int16":
				s := values[j].(string)
				vInt16, _ := strconv.ParseInt(s, 10, 16)
				elem.Field(i).Set(reflect.ValueOf(vInt16))
			case "int32":
				s := values[j].(string)
				vInt32, _ := strconv.ParseInt(s, 10, 32)
				elem.Field(i).Set(reflect.ValueOf(vInt32))
			case "int64":
				s := values[j].(string)
				vInt64, _ := strconv.ParseInt(s, 10, 64)
				elem.Field(i).Set(reflect.ValueOf(vInt64))
			case "float32":
				s := values[j].(string)
				vFloat32, _ := strconv.ParseFloat(s, 32)
				elem.Field(i).Set(reflect.ValueOf(vFloat32))
			case "float64":
				s := values[j].(string)
				vFloat64, _ := strconv.ParseFloat(s, 64)
				elem.Field(i).Set(reflect.ValueOf(vFloat64))
			case "string":
				elem.Field(i).Set(reflect.ValueOf(values[j].(string)))
			case "time.Time":
				s := values[j].(string)
				vTime, _ := time.Parse(time.RFC3339, s)
				elem.Field(i).Set(reflect.ValueOf(vTime))
			}
		}
	}

	return nil
}

// Count 统计
func (d *MySqlDB) Count(count *int, sql string, args ...interface{}) error {
	//执行查询
	row := d.QueryRow(sql, args...)
	if row.Err() != nil {
		logger.Log.Printf("d.QueryRow.err:%s", row.Err().Error())
		return row.Err()
	}

	err := row.Scan(&count)
	if err != nil {
		logger.Log.Printf("Count 取值出错：%s", err.Error())
		return err
	}
	return nil
}

// Save 新增
func (d *MySqlDB) Save(sql string, args ...interface{}) (int64, error) {
	ret, err := DB.Exec(sql, args...)
	if err != nil {
		logger.Log.Printf("insert err:%s", err.Error())
		return 0, err
	}
	id, err := ret.LastInsertId()
	if err != nil {
		logger.Log.Printf("ret.LastInsertId().err:%s", err.Error())
		return 0, err
	}
	return id, nil
}

// Update 修改
func (d *MySqlDB) Update(sql string, args ...interface{}) (int64, error) {
	ret, err := DB.Exec(sql, args...)
	if err != nil {
		logger.Log.Printf("update err:%s", err.Error())
		return 0, err
	}
	num, err := ret.RowsAffected()
	if err != nil {
		logger.Log.Printf("ret.RowsAffected().err:%s", err.Error())
		return 0, err
	}
	return num, nil
}

// Delete 删除
func (d *MySqlDB) Delete(sql string, args ...interface{}) (int64, error) {
	ret, err := DB.Exec(sql, args...)
	if err != nil {
		logger.Log.Printf("delete err:%s", err.Error())
		return 0, err
	}
	num, err := ret.RowsAffected()
	if err != nil {
		logger.Log.Printf("ret.RowsAffected().err:%s", err.Error())
		return 0, err
	}
	return num, nil
}
