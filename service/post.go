package service

import (
	"go-blog/dao"
	"go-blog/models"
	"go-blog/my-frame/config"
	"html/template"
	"log"
)

type PostService struct {
}

func (p *PostService) GetPostDetail(pid int) (*models.PostRes, error) {
	post, err := (&dao.PostDao{}).GetPostById(pid)
	if err != nil {
		log.Printf("dao.GetPostById.err:%s", err.Error())
		return nil, err
	}
	categoryName := (&dao.CategoryDao{}).GetCategoryNameById(post.CategoryId)
	userName := (&dao.UserDao{}).GetUserNameById(post.UserId)
	postMore := models.PostMore{
		Pid:          post.Pid,
		Title:        post.Title,
		Content:      template.HTML(post.Content),
		CategoryId:   post.CategoryId,
		CategoryName: categoryName,
		UserId:       post.UserId,
		UserName:     userName,
		ViewCount:    post.ViewCount,
		Type:         post.Type,
		Slug:         post.Slug,
		CreateAt:     models.DateDay(post.CreateAt),
		UpdateAt:     models.DateDay(post.UpdateAt),
	}
	var postRes = &models.PostRes{
		Viewer:       config.Cfg.Viewer,
		SystemConfig: config.Cfg.System,
		Article:      postMore,
	}
	return postRes, nil
}

func (p *PostService) Writing() (wr models.WritingRes) {
	categorys, err := (&dao.CategoryDao{}).GetAllCategory()
	if err != nil {
		log.Printf("dao.GetAllCategory.err:%s", err.Error())
		return
	}
	wr = models.WritingRes{
		Title:     config.Cfg.Viewer.Title,
		CdnURL:    config.Cfg.System.CdnURL,
		Categorys: categorys,
	}
	return wr
}

// SavePost 保存文章数据(新增)
func (p *PostService) SavePost(post *models.Post) {
	(&dao.PostDao{}).SavePost(post)
}

// UpdatePost 更新文章数据
func (p *PostService) UpdatePost(post *models.Post) {
	(&dao.PostDao{}).UpdatePost(post)
}

// SearchPost 搜索文章
func (p *PostService) SearchPost(condition string) []models.SearchResp {
	posts, _ := (&dao.PostDao{}).GetPostSearch(condition)
	var searchResps []models.SearchResp
	for _, post := range posts {
		searchResps = append(searchResps, models.SearchResp{
			post.Pid,
			post.Title,
		})
	}
	return searchResps
}

// GetPostById 根据id获取
func (p *PostService) GetPostById(pid int) (*models.Post, error) {
	return (&dao.PostDao{}).GetPostById(pid)
}
