package service

import (
	"go-blog/dao"
	"go-blog/models"
	"go-blog/my-frame/config"
	"html/template"
	"log"
)

func (p *PostService) GetAllIndexInfo(page, pageSize int) (*models.HomeResponse, error) {
	//获取分类信息
	categorys, err := (&dao.CategoryDao{}).GetAllCategory()
	if err != nil {
		log.Printf("获取分类信息失败：%s", err.Error())
		return nil, err
	}
	//获取文章分页信息
	posts, err := (&dao.PostDao{}).GetPostPage(page, pageSize)
	if err != nil {
		log.Printf("获取文章分页数据失败：%s", err.Error())
		return nil, err
	}
	var postMores []models.PostMore
	for _, post := range posts {
		//分类名称 【可优化点】
		categoryName := (&dao.CategoryDao{}).GetCategoryNameById(post.CategoryId)

		//用户名称 【可优化点】
		userName := (&dao.UserDao{}).GetUserNameById(post.UserId)

		//文章内容
		content := []rune(post.Content)
		//列表单条数据长度过长，进行截断之后显示
		if len(content) > 100 {
			content = content[:100]
		}
		postMore := models.PostMore{
			Pid:          post.Pid,
			Title:        post.Title,
			Slug:         post.Slug,
			Content:      template.HTML(content),
			CategoryId:   post.CategoryId,
			CategoryName: categoryName,
			UserId:       post.UserId,
			UserName:     userName,
			ViewCount:    post.ViewCount,
			Type:         post.Type,
			CreateAt:     models.DateDay(post.CreateAt),
			UpdateAt:     models.DateDay(post.UpdateAt),
		}
		postMores = append(postMores, postMore)
	}
	//计算分页总数
	//11 10 2 10 1 9 1 21 3
	//公式: (11-1)/10+1=2
	total := (&dao.PostDao{}).CountGetAllPost()
	pagesCount := (total-1)/pageSize + 1
	var pages []int
	for i := 0; i < pagesCount; i++ {
		pages = append(pages, i+1)
	}

	var hr = &models.HomeResponse{
		Viewer:    config.Cfg.Viewer,
		Categorys: categorys,
		Posts:     postMores,
		Total:     total,
		Page:      page,
		Pages:     pages,
		PageEnd:   page != pagesCount,
	}
	return hr, nil
}

func (p *PostService) GetAllIndexInfoBySlug(slug string, page, pageSize int) (*models.HomeResponse, error) {
	//获取分类信息
	categorys, err := (&dao.CategoryDao{}).GetAllCategory()
	if err != nil {
		log.Printf("获取分类信息失败：%s", err.Error())
		return nil, err
	}

	var posts []models.Post
	var total int
	if slug == "" {
		//获取文章分页信息
		posts, err = (&dao.PostDao{}).GetPostPage(page, pageSize)
		total = (&dao.PostDao{}).CountGetAllPost()
	} else {

		posts, err = (&dao.PostDao{}).GetPostPageBySlug(slug, page, pageSize)
		total = (&dao.PostDao{}).CountGetAllPostBySlug(slug)
	}
	if err != nil {
		log.Printf("获取文章分页数据失败：%s", err.Error())
		return nil, err
	}
	var postMores []models.PostMore
	for _, post := range posts {
		//分类名称 【可优化点】
		categoryName := (&dao.CategoryDao{}).GetCategoryNameById(post.CategoryId)

		//用户名称 【可优化点】
		userName := (&dao.UserDao{}).GetUserNameById(post.UserId)

		//文章内容
		content := []rune(post.Content)
		//列表单条数据长度过长，进行截断之后显示
		if len(content) > 100 {
			content = content[:100]
		}
		postMore := models.PostMore{
			Pid:          post.Pid,
			Title:        post.Title,
			Slug:         post.Slug,
			Content:      template.HTML(content),
			CategoryId:   post.CategoryId,
			CategoryName: categoryName,
			UserId:       post.UserId,
			UserName:     userName,
			ViewCount:    post.ViewCount,
			Type:         post.Type,
			CreateAt:     models.DateDay(post.CreateAt),
			UpdateAt:     models.DateDay(post.UpdateAt),
		}
		postMores = append(postMores, postMore)
	}
	//计算分页总数
	//11 10 2 10 1 9 1 21 3
	//公式: (11-1)/10+1=2

	pagesCount := (total-1)/pageSize + 1
	var pages []int
	for i := 0; i < pagesCount; i++ {
		pages = append(pages, i+1)
	}

	var hr = &models.HomeResponse{
		Viewer:    config.Cfg.Viewer,
		Categorys: categorys,
		Posts:     postMores,
		Total:     total,
		Page:      page,
		Pages:     pages,
		PageEnd:   page != pagesCount,
	}
	return hr, nil
}
