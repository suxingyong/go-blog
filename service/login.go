package service

import (
	"errors"
	"fmt"
	"go-blog/common/utils"
	"go-blog/dao"
	"go-blog/models"
	"log"
)

type UserService struct {
}

// Login 登录处理逻辑
// 这里拿到的密码可能是经过了一次加密的，需要注意配合前端代码一起来鉴别
// 目前前端是去掉了密码的加密的，所以是明文
func (u *UserService) Login(userName, passwd string) (*models.LoginResp, error) {
	log.Printf("userName:【%s】;passwd:【%s】,len(passwd):%d", userName, passwd, len(passwd))
	//对传入的密码进行md5加密
	passwd = utils.Md5Crypt(passwd, "123456")
	log.Printf("passwd:【%s】", passwd)
	user := (&dao.UserDao{}).GetUser(userName, passwd)
	if user == nil {
		msg := fmt.Sprintf("账号密码不正确")
		log.Printf("%s", msg)
		return nil, errors.New(msg)
	}
	uid := user.Uid
	//生成token jwt技术进行生成 令牌 A.B.C
	token, err := utils.Award(&uid)
	if err != nil {
		log.Printf("%v", err)
		return nil, errors.New("token未能生成")
	}
	var userInfo models.UserInfo
	userInfo.Uid = user.Uid
	userInfo.UserName = user.UserName
	userInfo.Avatar = user.Avatar
	var lr = &models.LoginResp{
		Token:    token,
		UserInfo: userInfo,
	}
	return lr, nil
}
