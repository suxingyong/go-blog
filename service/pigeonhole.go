package service

import (
	"go-blog/dao"
	"go-blog/models"
	"go-blog/my-frame/config"
)

func (p *PostService) FindPostPigeonhole() models.PigeonholeRes {
	//查询所有的文章 进行月份的整理
	posts, _ := (&dao.PostDao{}).GetPostAll()

	pigeonholeMap := make(map[string][]models.Post)
	for _, post := range posts {
		at := post.CreateAt
		month := at.Format("2006-01")
		pigeonholeMap[month] = append(pigeonholeMap[month], post)
	}

	//查询所有的分类
	categorys, _ := (&dao.CategoryDao{}).GetAllCategory()

	return models.PigeonholeRes{
		Viewer:       config.Cfg.Viewer,
		SystemConfig: config.Cfg.System,
		Categorys:    categorys,
		Lines:        pigeonholeMap,
	}

}
