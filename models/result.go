package models

// Result api接口返回结果
type Result struct {
	Code  int         `json:"code"`  //状态码
	Data  interface{} `json:"data"`  //数据
	Error string      `json:"error"` //错误
}
