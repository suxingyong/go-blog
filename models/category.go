package models

import "time"

// Category 分类
type Category struct {
	Cid      int       `json:"cid"`       //分类-id
	Name     string    `json:"name"`      //分类-名称
	CreateAt time.Time `json:"create_at"` //分类-创建时间
	UpdateAt time.Time `json:"update_at"` //分类-修改时间
}

// CategoryResponse 响应参数
type CategoryResponse struct {
	*HomeResponse
	CategoryName string
}
