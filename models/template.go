package models

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"
)

type TemplateBlog struct {
	*template.Template
}

type HtmlTemplate struct {
	Index      TemplateBlog
	Category   TemplateBlog
	Custom     TemplateBlog
	Detail     TemplateBlog
	Login      TemplateBlog
	Pigeonhole TemplateBlog
	Writing    TemplateBlog
}

func (t *TemplateBlog) WriteData(w http.ResponseWriter, hr interface{}) error {
	err := t.Execute(w, hr)
	if err != nil {
		log.Println("往模板写入数据失败：", err.Error())
		w.Write([]byte("error"))
	}
	return err
}

func (t *TemplateBlog) WriteError(w http.ResponseWriter, err error) {
	_, err = w.Write([]byte(err.Error()))
	if err != nil {
		log.Println(err)
		return
	}
}

func InitTemplate(templateDir string) (HtmlTemplate, error) {
	tp, err := readTemplate(
		[]string{"index", "category", "custom", "detail", "login", "pigeonhole", "writing"},
		templateDir,
	)
	var htmlTemplate HtmlTemplate
	if err != nil {
		log.Println("加载模板出错：", err)
		return htmlTemplate, err
	}
	htmlTemplate.Index = tp["index"]
	htmlTemplate.Category = tp["category"]
	htmlTemplate.Custom = tp["custom"]
	htmlTemplate.Detail = tp["detail"]
	htmlTemplate.Login = tp["login"]
	htmlTemplate.Pigeonhole = tp["pigeonhole"]
	htmlTemplate.Writing = tp["writing"]
	return htmlTemplate, err
}

func IsODD(num int) bool {
	return num%2 == 0
}

func GetNextName(strs []string, index int) string {
	return strs[index+1]
}

func Date(layout string) string {
	return time.Now().Format(layout)
}

func DateDay(date time.Time) string {
	return date.Format("2006-01-01 15:04:05")
}

func readTemplate(templates []string, templateDir string) (map[string]TemplateBlog, error) {
	var tbMap = make(map[string]TemplateBlog)

	for _, view := range templates {
		viewName := fmt.Sprintf("%s.html", view)

		t := template.New(viewName)

		//访问博客首页模板的时候，因为有多个模板的嵌套，解析文件的时候，需要将其涉及到的所有模板都进行解析
		var viewPaths []string

		viewPaths = append(viewPaths, fmt.Sprintf("%s/%s", templateDir, viewName))

		home := templateDir + "/home.html"
		viewPaths = append(viewPaths, home)

		header := templateDir + "/layout/header.html"
		viewPaths = append(viewPaths, header)

		footer := templateDir + "/layout/footer.html"
		viewPaths = append(viewPaths, footer)

		personal := templateDir + "/layout/personal.html"
		viewPaths = append(viewPaths, personal)

		post := templateDir + "/layout/post-list.html"
		viewPaths = append(viewPaths, post)

		pagination := templateDir + "/layout/pagination.html"
		viewPaths = append(viewPaths, pagination)

		t.Funcs(template.FuncMap{
			"isODD":       IsODD,
			"getNextName": GetNextName,
			"date":        Date,
			"dateDay":     DateDay,
		})
		t, err := t.ParseFiles(viewPaths...)
		if err != nil {
			log.Printf("解析模板出错：%s", err.Error())
			return nil, err
		}
		var tb TemplateBlog
		tb.Template = t
		tbMap[view] = tb
	}
	return tbMap, nil

}
