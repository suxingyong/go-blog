package models

// LoginResp 登录响应
type LoginResp struct {
	Token    string      `json:"token"`
	UserInfo interface{} `json:"userInfo"`
}
