package models

import "time"

// User 用户信息[数据库用]
type User struct {
	Uid      int       `json:"uid"`       //用户id
	UserName string    `json:"user_name"` //用户名
	Passwd   string    `json:"passwd"`    //密码
	Avatar   string    `json:"avatar"`    //别名
	CreateAt time.Time `json:"create_at"` //创建时间
	UpdateAt time.Time `json:"update_at"` //修改时间
}

type UserInfo struct {
	Uid      int    `json:"uid"`
	UserName string `json:"user_name"`
	Avatar   string `json:"avatar"`
}
