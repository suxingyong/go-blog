package models

import (
	"go-blog/my-frame/config"
	"html/template"
	"time"
)

// Post 文章[存储数据库用]
type Post struct {
	Pid        int       `json:"pid"`         //文章ID
	Title      string    `json:"title"`       //文章标题
	Content    string    `json:"content"`     //文章的html
	Markdown   string    `json:"markdown"`    //文件的Markdown
	CategoryId int       `json:"category_id"` //分类id
	UserId     int       `json:"user_id"`     //用户id
	ViewCount  int       `json:"view_count"`  //查看次数
	Type       int       `json:"type"`        //文件类型 [0-普通;1-自定义文章]
	Slug       string    `json:"slug"`        //自定义页面 path
	CreateAt   time.Time `json:"create_at"`   //创建时间
	UpdateAt   time.Time `json:"update_at"`   //更新时间
}

// PostMore 文章[返回给前端用]
type PostMore struct {
	Pid          int           `json:"pid"`           //文章ID
	Title        string        `json:"title"`         //文章标题
	Content      template.HTML `json:"content"`       //文章的html
	CategoryId   int           `json:"category_id"`   //分类id
	CategoryName string        `json:"category_name"` //分类名
	UserId       int           `json:"user_id"`       //用户id
	UserName     string        `json:"user_name"`     //用户名
	ViewCount    int           `json:"view_count"`    //查看次数
	Type         int           `json:"type"`          //文件类型 [0-普通;1-自定义文章]
	Slug         string        `json:"slug"`          //自定义页面 path
	CreateAt     string        `json:"create_at"`     //创建时间
	UpdateAt     string        `json:"update_at"`     //更新时间
}

// PostReq 文章[前端提交请求]
type PostReq struct {
	Pid        int    `json:"pid"`         //文章id
	Title      string `json:"title"`       //文章标题
	Slug       string `json:"slug"`        //文章自定义链接分类
	Content    string `json:"content"`     //文章内容
	Markdown   string `json:"markdown"`    //文章markdown格式的内容
	CategoryId int    `json:"category_id"` //文章分类id
	UserId     int    `json:"user_id"`     //文章用户id
	Type       int    `json:"type"`        //文章类别 [0-普通文章;1-精选文章]
}

// SearchResp 搜素响应结果
type SearchResp struct {
	Pid   int    `orm:"pid" json:"pid"`
	Title string `orm:"title" json:"title"`
}

type PostRes struct {
	config.Viewer
	config.SystemConfig
	Article PostMore
}

type WritingRes struct {
	Title     string
	CdnURL    string
	Categorys []Category
}

type PigeonholeRes struct {
	config.Viewer
	config.SystemConfig
	Categorys []Category
	Lines     map[string][]Post
}
