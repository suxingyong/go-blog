package models

import "go-blog/my-frame/config"

// HomeResponse 主页响应
type HomeResponse struct {
	config.Viewer
	Categorys []Category
	Posts     []PostMore
	Total     int
	Page      int
	Pages     []int
	PageEnd   bool
}
