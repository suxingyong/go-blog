package context

import (
	"encoding/json"
	"go-blog/my-frame/logger"
	"io/ioutil"
	"net/http"
	"regexp"
)

var Context = NewContext()

// MyContext 上下文
type MyContext struct {
	Request  *http.Request                   //http请求
	W        http.ResponseWriter             //http响应
	routers  map[string]func(ctx *MyContext) //路由
	pathArgs map[string]map[string]string    //路径参数
}

func NewContext() *MyContext {
	ctx := &MyContext{}
	ctx.routers = make(map[string]func(ctx2 *MyContext))
	ctx.pathArgs = make(map[string]map[string]string)
	return ctx
}

func (ctx *MyContext) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx.W = w
	ctx.Request = r
	path := r.URL.Path
	f := ctx.routers[path]
	if f == nil {
		for key, f1 := range ctx.routers {
			//判断是否为携带路径参数的
			reg, _ := regexp.Compile("(/\\w+)*(/{\\w+})+(/\\w+)*")
			match := reg.MatchString(key)
			if !match {
				continue
			}
			isHav, args := UrlTree.Search(path)
			if isHav {
				//匹配上 存储路径对应参数
				ctx.pathArgs[path] = args
				f1(ctx)
			}
		}
	} else {
		f(ctx)
	}
}

// Handler 处理器
func (ctx *MyContext) Handler(url string, f func(ctx *MyContext)) {
	UrlTree.Insert(url)
	ctx.routers[url] = f
}

// GetPathVariable 获取路径变量
func (ctx MyContext) GetPathVariable(key string) string {
	return ctx.pathArgs[ctx.Request.URL.Path][key]
}

// GetForm 获取表单参数
func (ctx *MyContext) GetForm(key string) (string, error) {
	if err := ctx.Request.ParseForm(); err != nil {
		logger.Log.Printf("表单获取失败：", err)
		return "", err
	}
	return ctx.Request.Form.Get(key), nil
}

// GetJson 获取json
func (ctx *MyContext) GetJson(key string) interface{} {
	var params map[string]interface{}
	bodyBytes, err := ioutil.ReadAll(ctx.Request.Body)
	if err != nil {
		logger.Log.Printf("读取json失败，err:%s", err.Error())
		return nil
	}
	err = json.Unmarshal(bodyBytes, &params)
	if err != nil {
		logger.Log.Printf("json格式换失败，err:%s", err.Error())
		return nil
	}
	return params[key]
}
