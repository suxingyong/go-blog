package context

import (
	"go-blog/my-frame/logger"
	"strings"
)

var UrlTree = NewTrie()

// Trie 前缀树结构 用于路劲参数匹配
type Trie struct {
	next   map[string]*Trie
	isWord bool
}

func NewTrie() Trie {
	root := new(Trie)
	root.next = make(map[string]*Trie)
	root.isWord = false
	return *root
}

// Insert 插入数据，路由根据 "/" 进行拆分
func (t *Trie) Insert(word string) {
	for _, v := range strings.Split(word, "/") {
		if t.next[v] == nil {
			node := new(Trie)
			node.next = make(map[string]*Trie)
			node.isWord = false
			t.next[v] = node
		}
		// * 匹配所有
		// {X} 匹配路由参数 X
		if v == "*" || strings.Index(v, "{") != -1 {
			t.isWord = true
		}
		t = t.next[v]
	}
	t.isWord = true
}

// Search 匹配路由
func (t *Trie) Search(word string) (bool, map[string]string) {
	isHave := false
	arg := make(map[string]string)

	for _, v := range strings.Split(word, "/") {
		if t.isWord {
			for k, _ := range t.next {
				if strings.Index(k, "{") != -1 {
					key := strings.Replace(k, "{", "", -1)
					key = strings.Replace(k, "}", "", -1)
					arg[key] = v
				}
				v = k
			}
			logger.Log.Printf("v=%v", v)
		}
		if t.next[v] == nil {
			logger.Log.Printf("找不到了，匹配不上")
			return isHave, arg
		}
		t = t.next[v]
	}
	logger.Log.Printf("t.next:%v,len(t.next):%v", t.next, len(t.next))
	//必须匹配全 比如： /v1/{b}/{a}    /v1/123 匹配不到， /v1/123/456才可匹配
	if len(t.next) == 0 {
		isHave = t.isWord
		return isHave, arg
	}
	return isHave, arg
}
