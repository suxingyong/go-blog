package server

import (
	"fmt"
	"go-blog/my-frame/logger"
	"go-blog/router"
	"net/http"
)

var App = &MyServer{}

type MyServer struct {
}

// Start 服务启动
func (m *MyServer) Start(ip, port string) {
	server := http.Server{
		Addr: fmt.Sprintf("%s:%s", ip, port),
	}
	//路由
	router.Router()
	if err := server.ListenAndServe(); err != nil {
		logger.Log.Printf("server.ListenAndServe().err:%s", err.Error())
		panic(err)
	}
	logger.Log.Printf("服务启动成功:%s", fmt.Sprintf("%s:%s", ip, port))
}
