package api

import (
	"go-blog/common"
	"go-blog/service"
	"log"
	"net/http"
)

func (a *Api) Login(w http.ResponseWriter, r *http.Request) {
	//接收用户名和密码 返回 对应的json
	params := common.GetRequestJsonParam(r)

	userName := params["username"].(string)
	passwd := params["passwd"].(string)
	loginRes, err := (&service.UserService{}).Login(userName, passwd)
	if err != nil {
		log.Printf("service.Login.err:%s", err.Error())
		common.Error(w, err)
		return
	}

	common.Success(w, loginRes)
}
