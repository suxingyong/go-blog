package api

import (
	"errors"
	"fmt"
	"go-blog/common"
	"go-blog/common/utils"
	"go-blog/models"
	"go-blog/service"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// GetPost 获取详情(更新时使用)
func (a *Api) GetPost(w http.ResponseWriter, r *http.Request) {
	//获取路径参数
	path := r.URL.Path
	pIdStr := strings.TrimPrefix(path, "/api/v1/post/")
	pid, err := strconv.Atoi(pIdStr)
	if err != nil {
		msg := "不识别此请求路径"
		log.Printf("%s,err:%s", msg, err.Error())
		common.Error(w, errors.New(msg))
		return
	}
	postRes, err := (&service.PostService{}).GetPostById(pid)
	if err != nil {
		msg := "查询出错"
		log.Printf("%s,err:%s", msg, err.Error())
		common.Error(w, errors.New(msg))
		return
	}
	common.Success(w, postRes)
}

// SaveAndUpdatePost 保存或者更新
func (a *Api) SaveAndUpdatePost(w http.ResponseWriter, r *http.Request) {
	//获取用户id,判断用户是否登录
	token := r.Header.Get("Authorization")
	_, claim, err := utils.ParseToken(token)
	if err != nil {
		log.Printf("utils.ParseToken.err:%s", err.Error())
		common.Error(w, errors.New("登录已过期"))
		return
	}

	uid := claim.Uid

	//解析请求参数
	params := common.GetRequestJsonParam(r)

	//依次获取参数
	cId := params["categoryId"].(string)
	categoryId, _ := strconv.Atoi(cId)
	content := params["content"].(string)
	markdown := params["markdown"].(string)
	slug := params["slug"].(string)
	title := params["title"].(string)

	pType := 0
	if postType, isExist := params["type"]; isExist {
		pType = int(postType.(float64))
	}

	// 构建结构体
	post := &models.Post{
		Title:      title,
		Content:    content,
		Markdown:   markdown,
		CategoryId: categoryId,
		UserId:     uid,
		ViewCount:  0,
		Type:       pType,
		Slug:       slug,
		CreateAt:   time.Now(),
		UpdateAt:   time.Now(),
	}

	//POST save
	method := r.Method
	switch method {
	case http.MethodPost:
		//新增

		//保存数据
		(&service.PostService{}).SavePost(post)
		//返回成功
		common.Success(w, post)

	case http.MethodPut:
		//更新
		pIdFloat := params["pid"].(float64)
		pId := int(pIdFloat)

		createAtStr := params["create_at"].(string)

		createAt, err := time.ParseInLocation(time.RFC3339, createAtStr, time.Local)
		if err != nil {
			msg := fmt.Sprintf("时间格式转换错误")
			log.Printf("time.ParseInLocation.err:%s", err.Error())
			common.Error(w, errors.New(msg))
		}

		post.Pid = pId
		post.CreateAt = createAt

		(&service.PostService{}).UpdatePost(post)
		common.Success(w, post)
	}
}

// SearchPost 搜索
func (a *Api) SearchPost(w http.ResponseWriter, r *http.Request) {
	_ = r.ParseForm()
	condition := r.Form.Get("val")
	searchResp := (&service.PostService{}).SearchPost(condition)
	common.Success(w, searchResp)
}
